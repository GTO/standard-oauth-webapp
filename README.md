## Just trying to understand this protocol

A landing page that provide just three buttons :
 - Connect with Google
 - Connect with Twitter
 - Connect with Facebook

Once connected, the web app displays standard information such as :
 - email
 - name
 - last name

These informations should be used as a starting point to a social app
